//package Threads.VolatileExample;
//
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//class MyValue {
//    private long value = 0;
//
//    public void incrementValue() {
//        this.value++;
//    }
//
//    public long getValue() {
//        return value;
//    }
//}
//
//class MyThread implements Runnable {
//    private String name;
//    static MyValue value = new MyValue();
//
//    public MyThread(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public void run() {
//        for(int i=0; i<10; i++) {
//            value.incrementValue();
//            System.out.println(name + ": " + value.getValue());
//        }
//    }
//}
//
//public class VolatileExample {
//    public static void main(String[] args) {
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.execute(new MyThread("Thread 1"));
//        executorService.execute(new MyThread("Thread 2"));
//        executorService.shutdown();
//    }
//}

package Threads.VolatileExample;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VolatileExample implements Runnable {

    private String name;

    private static class ValueVolatile {

        private static volatile long value;

        public static long getValue() {
            return value;
        }

        public static void setValue(long value) {
            ValueVolatile.value = value;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VolatileExample(String name) {
        this.setName(name);
    }

    public void run() {
        for (int i = 1; i < 10; i++) {
            long x = 11111111111111L * i;
            System.out.println("Odczyt " + this.getName() + ": " + ValueVolatile.getValue());
            ValueVolatile.setValue(x);
            System.out.println("Zapis " + this.getName() + ": " + ValueVolatile.getValue());
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new VolatileExample("Watek 1"));
        executorService.execute(new VolatileExample("Watek 2"));
        executorService.shutdown();

    }

}
