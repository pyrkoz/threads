package Threads;

class ValueClass {
    private long value = 0;

    public synchronized void increment() {
        value +=1;
    }

    public long getValue() {
        return value;
    }
}

class Producer extends Thread{
    private final ValueClass value;

    public Producer(ValueClass value) {
        this.value = value;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                synchronized (value) {
                    value.increment();
                    System.out.println("Producent increment: " + value.getValue());
                    value.notify();
                    value.wait();
                }
            }
        }
        catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

class Consumer extends Thread{
    private final ValueClass value;

    public Consumer(ValueClass value) {
        this.value = value;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                synchronized (value) {
                    value.wait();
                    value.increment();
                    System.out.println("Reader increment: " + value.getValue());
                    value.notify();
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

public class Main {
    public static void main(String[] args) {
        ValueClass value = new ValueClass();
        Producer producer = new Producer(value);
        Consumer consumer = new Consumer(value);
        consumer.start();
        producer.start();

    }
}

//
